Gem::Specification.new do |s|
  s.name = 'locale_types'
  s.version = '0.1.0'
  s.date = '2017-10-05'
  s.summary = 'ActiveModel types for locale and time zone.'
  s.description = 'ActiveModel types for locale and time zone.'
  s.authors = ['Zyrthofar']
  s.email = 'zyrthofar@gmail.com'
  s.files = [
    'lib/locale_types.rb',
    'lib/active_model/type/locale.rb',
    'lib/active_model/type/time_zone.rb',
    'lib/active_record/type/locale.rb',
    'lib/active_record/type/time_zone.rb',
  ]
  s.license = 'MIT'
end

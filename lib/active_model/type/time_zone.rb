module ActiveModel
  module Type
    class TimeZone < Value
      def type
        :time_zone
      end

      def cast_value(value)
        available_time_zone(value)
      end

      def serialize(value)
        available_time_zone(value)&.name
      end

      private

      def available_time_zone(time_zone)
        return nil if time_zone.nil?
        return time_zone if time_zone.is_a?(ActiveSupport::TimeZone)

        if ActiveSupport::TimeZone.all.map(&:name).include?(time_zone)
          ActiveSupport::TimeZone[time_zone]
        end
      end
    end
  end
end

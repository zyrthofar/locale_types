module ActiveModel
  module Type
    class Locale < Value
      def type
        :locale
      end

      def cast_value(value)
        if I18n.locale_available?(value)
          value.to_sym
        else
          nil
        end
      end

      def serialize(value)
        if I18n.locale_available?(value)
          value.to_s
        else
          nil
        end
      end
    end
  end
end

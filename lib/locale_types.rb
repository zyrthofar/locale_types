require 'active_model/type/locale'
require 'active_model/type/time_zone'
require 'active_record/type/locale'
require 'active_record/type/time_zone'

ActiveRecord::Type.register(:locale, ActiveRecord::Type::Locale)
ActiveRecord::Type.register(:time_zone, ActiveRecord::Type::TimeZone)
